/**==================== Ejercicio #1 ====================*/
/* ======== ¿Cuánto dan las siguientes expresiones? ==== */
/*
const nombres = ["Juan", "Pedro", "Wally", "Sofía", "Amelia", "Estefanía"];

nombres.length; // 6
nombres[2];     // Wally
nombres[6];     // undefined
nombres[-10];   // undefined
*/
/**======================================================*/



/**==================== Ejercicio #2 ====================*/
/* ================ ¿Dónde está Wally? ================= */

// ======== Utilizando Bucle y condicional
/*
const nombres = ["Juan", "Pedro", "Wally", "Sofía", "Amelia", "Estefanía"];
let i = 0; // Tengo que declarar la variable fuera del For porque no me la toma

for (let i = 0; i < nombres.length; i++) {
    if (nombres[i] == "Wally") {
        console.log(`Wally te encontré, estas en la posición ${i} del Array`);
    }
}
*/
// ======== Utilizando el metodo indexOf()
/*
const nombres = ["Juan", "Pedro", "Wally", "Sofía", "Amelia", "Estefanía"];
console.log(`Wally te encontré, estas en la posición ${nombres.indexOf("Wally")} del Array`);
*/
/**======================================================*/



/**==================== Ejercicio #3 ====================*/
/* ==================== Último valor =================== */
/*
const mascotas = ["Perro", "Gato", "Axolote"];

//Item #1

console.log(`El último valor del Array es ${mascotas[(mascotas.length)-1]}`);

//Item #2

mascotas.push("Ornitorrinco");
console.log(`El último valor del Array ahora es ${mascotas[(mascotas.length)-1]}`);
*/
/**======================================================*/



/**==================== Ejercicio #4 ====================*/
/* ==================== Lista Infinita =================== */
/*
let productos = []; 

for (var i = 0; productos.length < 5; i++){
    productos[i] = prompt(`Ingrese el producto ${i+1}`);
}

console.log(productos);
*/
/**======================================================*/



/**==================== Ejercicio #5 ====================*/
/* ==================== Números Pares =================== */
/*
let resultado = [];
let j = 0;


for (var i = 10; i < 21; i++) {
    if (i % 2 == 0) {
        resultado[j] = i;
        j++;
    }
}
console.log(resultado);
*/
/**======================================================*/



/**==================== Ejercicio #6 ====================*/
/* ==============  ¿Dónde está Wally II? =============== */
/*
const nombres = ["Juan", "Pedro", "Wally", "Sofía", "Amelia", "Wally"];

let i = 0; // Tengo que declarar la variable fuera del For porque no me la toma
let j = 0;

const resultado = [];

for (let i = 0; i < nombres.length; i++) {
    if (nombres[i] == "Wally") {
        resultado[j] = i;
        j++;
    }
}
console.log(`Wally se encunetra en los indices ${resultado} del Array`);
*/
/**======================================================*/



/**==================== Ejercicio #7 ====================*/
/* ================  Números Pares II =========)======== */
/*
const NumberList = [147, 85, 95, 12, 8, 123, 65, 32, 53, 951];
const ResultList = [];

for (var i = 0; i < NumberList.length; i++) {
    if (NumberList[i] % 2 == 0) {
        ResultList[i] = "par";
    } else {
        ResultList[i] = "impar";
    }
}

for (var j = 0; j < ResultList.length; j++) {
    console.log(`El número ${NumberList[j]} es ${ResultList[j]}`);
}
*/
/**======================================================*/



/**==================== Ejercicio #8 ====================*/
/* =================== Suma de valores ================= */
/*
let ListaNumeros = [10, 20, 30, 40, 50];
let ResultadoSuma = 0;

for (var i = 0; i < ListaNumeros.length; i++){
    ResultadoSuma = ResultadoSuma + ListaNumeros[i];
}

console.log(`La suma de todos los elementos del Array es: ${ResultadoSuma}`)
*/
/**======================================================*/



/**==================== Ejercicio #9 ====================*/
/* ====================== Saludo ======================= */
/*
let ListaNombres = [];

for (var i = 0; ListaNombres.length < 10; i++){
    ListaNombres[i] = prompt(`Ingrese el Nombre ${i+1}`);
}

console.group("Saludos")
for (var j = 0; j < ListaNombres.length; j++){
    console.log(`Hola ${ListaNombres[j]}`)
}
console.groupEnd();
*/
/**======================================================*/



/**==================== Ejercicio #10 ====================*/
/* ============== Cantidad de Caracteres =============== */
/*
let destinos = ["Bariloche", "Santiago de Compostela", "Republica Dominicana", "Emiratos Arabes Unidos"];

for (var i = 0; i < destinos.length; i++){
    if (destinos[i].length == 20){
        console.log("Se excedió la cantidad de caracteres para el título de las cards");
    }
}
*/
/**======================================================*/



/**==================== Ejercicio #11 ====================*/
/* =============== Cuarentena y Descuentos ============= */
/*

let products = ["Vino", "Jamón", "Leche", "Queso", "Carne", "Aceite", "Aceitunas" ];

let price = [230, 320, 80, 450, 560, 90, 120];

let Total = 0;

console.group("Total Compra");
for (var i = 0; i < 7; i++){

    Total = Total + price[i];
    console.log(`${products[i]} ----> $${price[i]}`);
}
    console.log(`Total a pagar ----> $${Total}`);
console.groupEnd();

let cupon = prompt("Ingrese cupon de descuento");

if (cupon == "DTO10"){  // Aplica descuento de 10%
    console.log(`Total - 10% de descuento ---->$${(Total-(Total*0.10)).toFixed(2)}`);
}

if (cupon == "DTO15"){  // Aplica descuento de 15%
    console.log(`Total - 10% de descuento ---->$${(Total-(Total*0.15)).toFixed(2)}`);
}

if (cupon == "DTO20"){  // Aplica descuento de 20%
    console.log(`Total - 10% de descuento ---->$${(Total-(Total*0.20)).toFixed(2)}`);
}

*/
/**======================================================*/

/**==================== Ejercicio #12 ====================*/
/* ================== Salón de Fiestas ================== */
/*
const ListaIntegrantes = [["luis", "torres"], ["juan", "rodriguez"], ["lucia", "fernandez"], ["veronica", "landaeta"], ["lautaro", "martinez"], ["ignacio", "fuenmayor"], ["carlos", "tomasetto"], ["julio", "ramirez"], ["cristina", "moreno"], ["estefania", "triller"], ["pablo", "perez"]];

const personasEnElSalon = [];



function busqueda(string1, string2, number1, Array) { // parametros (nombre, apellido, longitud_Array, Array)
    for (var i = 0; i < number1; i++) {

        if (Array[i][0] == string1) {

            let name = true;

            if (Array[i][1] == string2) {

                let lastname = true;

                if (name && lastname) {

                    let answer = [true, i]; // Retorna un vector si la persona esta en la lista y posición del array donde se encuentra

                    return answer;
                }
            }
        }
    }

    return false; // En caos de que no este en la lista retorna "False"
}


while (true) {

    let accion1 = prompt("¿Qué desea hacer?")

    if (accion1 === "ingresar") {

        let nombre = (prompt("Ingrese Nombre")).toLowerCase();
        let apellido = (prompt("Ingrese Apellido")).toLowerCase();

        var resultado1 = busqueda(nombre, apellido, ListaIntegrantes.length, ListaIntegrantes);

        if (resultado1[0]) {

            alert(`${ListaIntegrantes[resultado1[1]]} puede ingresar`);
            personasEnElSalon.push(ListaIntegrantes[resultado1[1]]);
        } else {
            alert("Esta persona no esta en la lista");
        }

    }

    if (accion1 === "retirar") {

        let nombre = (prompt("Ingrese Nombre")).toLowerCase();
        let apellido = (prompt("Ingrese Apellido")).toLowerCase();

        var resultado2 = busqueda(nombre, apellido, personasEnElSalon.length, personasEnElSalon);

        if (resultado2[0]) {

            alert(`${personasEnElSalon[resultado2[1]]} puede retirarse`);
        } else {
            alert("Esta persona no esta en el salón");
        }

    }

    if (accion1 === "estado") {

        console.log(personasEnElSalon);
    }

}
*/
/**======================================================*/


/**==================== Ejercicio #13 ====================*/
/* ========= Base de Datos y Caracteres extraños ========= */
/*
const ListaDesordenada = ["Tengo 18 años", "23", "18 años", "20 años y 30 dias", "21 años y un poco más", "27 años y medio", "estoy llegando a los 18, me faltan unos meses", "Tengo 17 pero en unos días cumplo 18"];

const ListaOrdenada = [];

let numeros = "0123456789";
let captura = "";

for (var i = 0; i < ListaDesordenada.length; i++) {
    for (var j = 0; j < ListaDesordenada[i].length; j++) {
        if (numeros.indexOf(ListaDesordenada[i].charAt(j), 0) != -1) {
            captura = captura + ListaDesordenada[i].charAt(j);
        }
        if (captura.length > 0 && ListaDesordenada[i].charAt(j) == " ") {
            break;
        }
    }
    ListaOrdenada[i] = Number(captura);
    captura = "";
}
*/
/**======================================================*/